"Edit Text" REST Webservice
==========================
This is a simple practice in creating a REST web service with Java and the Jersey framework.
You can use two different services. One that change text size on the sample html text and one that change text colour.


How to use - instructions
--------------------
1. Import the dynamic web project to Eclipse.
2. Run on your preferred application server (ex Tomcat or GlassFish).
3. use the following uri for different results:


change size on text:
http://localhost:8080/REST_WS_Edit_text/rest/TextSizeService/result/500 (this gives text size 500% but you can change to any text size).

change color on text:
http://localhost:8080/REST_WS_Edit_text/rest/TextColorService/result/red (this gives colour red but all HTML basic colours are available).

 






