package services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path ("TextSizeService") 
public class TextSizeService {

	@GET
	@Path ("/result/{textsize}")
	@Produces (MediaType.TEXT_HTML)
	public String changeTextSize (@PathParam("textsize") String textsize) {
		
		
		return "<p style='font-size:" + textsize + "%;'> I'm the text! </p>";
		
	}
	
}
