package services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path ("TextColorService") 
public class TextColorService {

	@GET
	@Path ("/result/{textcolor}")
	@Produces (MediaType.TEXT_HTML)
	public String changeTextColor (@PathParam("textcolor") String textcolor) {
		
		
		return "<p style='font-size: 200%; color: " + textcolor + ";'> I'm the text! </p>";
		
	}
	
}
